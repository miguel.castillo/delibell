# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms


class OrderForm(forms.Form):
    order_name = forms.CharField(label='Name', max_length=10)
    order_room = forms.CharField(label='Room', max_length=5)
    order_foodchain = forms.CharField(label='Food Chain', max_length=16)
