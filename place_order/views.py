# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User

from .forms import OrderForm
from signup.models import Profile

from delibell.settings import RPI_SERVER_IP
from deli_log.rpi_server_logging import *

from deli_log.ws_server_logging import *
from channels import Group

import sys
import socket
import select
import threading

import time

SOCKET_LIST = []
RECV_BUFFER = 4096
PORT = 9019

global RPI_LOG_STARTED, WS_LOG_STARTED
RPI_LOG_STARTED = False
WS_LOG_STARTED = False

global client_socket
client_socket = None

global RPI_SERVER_STARTED, RPI_CONNECTED
RPI_SERVER_STARTED = False
RPI_CONNECTED = False

# Create your views here.
@login_required
def place_order(request):
    sys.stdout.write("(place_order : %s) Entered\n" % request.user.username)
    
    # Initialize logs
    global RPI_LOG_STARTED
    if RPI_LOG_STARTED is False:
        rpi_log_init()
        RPI_LOG_STARTED = True

    global WS_LOG_STARTED
    if WS_LOG_STARTED is False:
        ws_log_init()
        WS_LOG_STARTED = True

    # Establish connection with RPi if no connection has been made yet
    # if init_rpi_server_socket() != -1:
    #     listener = threading.Thread(target=run_rpi_server)
    #     listener.daemon = True
    #     listener.start()
    global RPI_SERVER_STARTED
    if RPI_SERVER_STARTED is False:
        init_rpi_server_socket()
        listener = threading.Thread(target=run_rpi_server)
        listener.daemon = True
        listener.start()
        RPI_SERVER_STARTED = True
    else:
        rpi_server_log("[RPI-SERVER] Socket already connected. no need to create.")

    user_profile = Profile.objects.get(user=request.user)

    if user_profile.has_order:
        return redirect('wait_order')
    else:
        # print user_profile.email_confirmed
        # print request.user.profile.email_confirmed
        if request.method == 'POST':
            form = OrderForm(request.POST)
            if form.is_valid():
                user_profile.order_name = form.cleaned_data.get("order_name")
                user_profile.order_room = form.cleaned_data.get("order_room")
                user_profile.order_foodchain = form.cleaned_data.get("order_foodchain")
                # MESSAGE = "MSG " + form.cleaned_data.get("order_name") + " " + form.cleaned_data.get("order_room") + " " + form.cleaned_data.get("order_foodchain")
                user_profile.order_message = create_order_message(
                    user_profile.user.username,
                    user_profile.order_name,
                    user_profile.order_room,
                    user_profile.order_foodchain)
                user_profile.save()
                # print "created message: " + MESSAGE
                return redirect('confirm_order')
        else:
            form = OrderForm()
        return render(request, 'place_order.html', {'form': form, 'profile': user_profile})

@login_required
def confirm_order(request):
    #return HttpResponse(MESSAGE)
    user_profile = Profile.objects.get(user=request.user)
    
    return render(request, 'confirm_order.html', {
            # 'ORDER_NAME': user_profile.order_name,
            # 'ORDER_ROOM': user_profile.order_room,
            # 'ORDER_FOODCHAIN': user_profile.order_foodchain,
            'profile' : user_profile,
        })

@login_required
def wait_order(request):
    user_profile = Profile.objects.get(user=request.user)
    # print user_profile.has_order
    user_profile.has_order = True
    # print user_profile.has_order
    if not user_profile.order_sent_to_rpi:
        rpi_server_log("( %s ) SENDING MESSAGE TO RPI: %s" % (request.user.username, user_profile.order_message))
        # INSERT SEND MESSAGE TO RPI
        # global client_socket
        #if client_socket is None:
        #if send_data(user_profile.order_message) == -1:
        global RPI_CONNECTED
        if RPI_CONNECTED is False:
            rpi_server_log("( %s ) RPI OFFLINE" % (request.user.username))
            user_profile.save()
            return render(request,'rpi_offline.html', {
                'profile' : user_profile,
                })
        send_data(user_profile.order_message)
        
        # print user_profile.order_sent_to_rpi
        user_profile.order_sent_to_rpi = True
        # print user_profile.order_sent_to_rpi
    user_profile.save()
    # print user_profile.has_order
    # print user_profile.order_sent_to_rpi
    # print user_profile.has_order
    return render(request, 'wait_order.html', {
            # 'ORDER_NAME': user_profile.order_name,
            # 'ORDER_ROOM': user_profile.order_room,
            # 'ORDER_FOODCHAIN': user_profile.order_foodchain,
            # 'ORDER_ARRIVED' : user_profile.order_arrived,
            'profile' : user_profile,
        })

@login_required
def remove_order(request):
    user_profile = Profile.objects.get(user=request.user)
    reset_profile_values(user_profile)
    return render(request, 'cancel_order.html')


@login_required
def cancel_order(request):
    user_profile = Profile.objects.get(user=request.user)
    DEL_MESSAGE = "DELETE " + user_profile.user.username
    rpi_server_log("( %s ) SENDING MESSAGE TO RPI: %s" % (request.user.username, DEL_MESSAGE))
    reset_profile_values(user_profile)
    global RPI_CONNECTED
    if RPI_CONNECTED is True:
        send_data(DEL_MESSAGE)
    else:
        rpi_server_log("<!---URGENT---!> ( %s ) RPI OFFLINE DURING CANCEL. REMOVE ORDER MANUALLY" % (request.user.username))
    return render(request, 'cancel_order.html')

@login_required
def order_arrived(request):
    user_profile = Profile.objects.get(user=request.user)
    ACKPING_MESSAGE = "ACKPING " + user_profile.user.username
    rpi_server_log("( %s ) SENDING MESSAGE TO RPI: %s" % (request.user.username, ACKPING_MESSAGE))
    reset_profile_values(user_profile)
    send_data(ACKPING_MESSAGE)
    return render(request, 'order_arrived.html')

@login_required
def wrong_order(request):
    user_profile = Profile.objects.get(user=request.user)
    user_profile.order_arrived = False
    user_profile.save()
    return redirect('wait_order')

def reset_profile_values(profile):
    profile.order_name = Profile._meta.get_field('order_name').get_default()
    profile.order_room = Profile._meta.get_field('order_room').get_default()
    profile.order_foodchain = Profile._meta.get_field('order_foodchain').get_default()
    profile.order_message = Profile._meta.get_field('order_message').get_default()
    profile.has_order = Profile._meta.get_field('has_order').get_default()
    profile.order_sent_to_rpi = Profile._meta.get_field('order_sent_to_rpi').get_default()
    profile.order_seen_by_rpi = Profile._meta.get_field('order_seen_by_rpi').get_default()
    profile.order_arrived = Profile._meta.get_field('order_arrived').get_default()
    profile.save()

# global stop_rpi_server
# stop_rpi_server = False

def run_rpi_server():
    # global stop_rpi_server
    # while not stop_rpi_server:
    while True:
        try:
            ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)

            for sock in ready_to_read:
                # new connection request
                if sock == server_socket:
                    global client_socket, RPI_CONNECTED
                    client_socket, addr = server_socket.accept()
                    SOCKET_LIST.append(client_socket)
                    rpi_server_log("[RPI-SERVER] RPi (%s, %s) connected" % addr)
                    RPI_CONNECTED = True
                #elif sock != sys.stdin:
                else:
                    # process data received
                    try:
                        data = sock.recv(RECV_BUFFER)
                        if data:
                            rpi_server_log("[RPI-SERVER] From RPi: " + str(data))
                            time.sleep(2)
                            # get token and user
                            recv_data_parse = str(data).partition(' ')
                            recv_data_token = recv_data_parse[0]
                            if recv_data_token == 'ACKMSG':
                                recv_data_username = recv_data_parse[2].partition(' ')[0]
                                recv_data_order_message = 'MSG ' + recv_data_parse[2]
                                recv_data_user = User.objects.get(username=recv_data_username)
                                user_profile = Profile.objects.get(user=recv_data_user)
                                #print "user_profile: " + user_profile.order_name
                                #print user_profile.order_message
                                if user_profile.order_message == recv_data_order_message:   
                                    user_profile.order_seen_by_rpi = True
                                    rpi_server_log("[RPI-SERVER] RPi received order of user " + str(user_profile))
                                    # Send SEEN to user
                                    ws_server_log("[WEBSOCKET SERVER] Sending SEEN to user " + str(user_profile))
                                    Group("delibellers").send({
                                        "text": "%s SEEN" % str(user_profile),
                                        })
                                user_profile.save()
                            elif recv_data_token == 'PING':
                                recv_data_username = str(data).partition(' ')[2]
                                recv_data_user = User.objects.get(username=recv_data_username)
                                user_profile = Profile.objects.get(user=recv_data_user)
                                if not user_profile.order_arrived:
                                    user_profile.order_arrived = True
                                    rpi_server_log("[RPI-SERVER] Order for user %s is here!" % str(user_profile))
                                    # send ping to user
                                    ws_server_log("[WEBSOCKET SERVER] Sending PING to user %s" % str(user_profile))
                                    Group("delibellers").send({
                                        "text": "%s PING" % str(user_profile),
                                        })
                                else:
                                    rpi_server_log("[RPI-SERVER] Rpi buzzed order for user %s!!" % str(user_profile))
                                    # send buzz to user
                                    ws_server_log("[WEBSOCKET SERVER] Sending BUZZ to user %s" % str(user_profile))
                                    Group("delibellers").send({
                                        "text": "%s BUZZ" % str(user_profile),
                                        })

                                user_profile.save()
                            
                        else:
                            if sock in SOCKET_LIST:
                                rpi_server_log("[RPI-SERVER] RPi disconnected")
                                SOCKET_LIST.remove(sock)
                                RPI_CONNECTED = False
                    except Exception as e:
                        rpi_server_log("[RPI-SERVER] data received but cannot process")
                        rpi_server_log(e)
                        continue
        except Exception as e:
            rpi_server_log(e)
            continue

    rpi_server_log("[RPI-SERVER] Closing rpi server socket...")
    server_socket.close()


def send_data (message):
    global client_socket
    try:
        client_socket.send(message)
    except Exception as e:
        #broken connection
        rpi_server_log("[RPI-SERVER] send_data: broken connection")
        rpi_server_log(e)
        if client_socket is not None:
            client_socket.close()
        return -1


def init_rpi_server_socket():
    # if server_port_is_in_use():
    #     rpi_server_log("[RPI-SERVER] Socket already connected. no need to create.")
    #     return -1

    #rpi_log_init()
    # we'll add this as well because we want initializing of logger to be done once
    #ws_log_init()

    global server_socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((RPI_SERVER_IP,PORT))
    server_socket.listen(5)

    # add server socket to list of readable connections
    SOCKET_LIST.append(server_socket)

    rpi_server_log("[RPI-SERVER] RPi server started on port " + str(PORT) + " and hostname " + str(RPI_SERVER_IP))
    
    # global stop_rpi_server
    # stop_rpi_server = False

def create_order_message(user,name,room,foodchain):
    num_padded_zeroes = 15 - len(name) - len(room)
    message_top = name + " " + room
    for i in range (num_padded_zeroes):
        message_top = message_top + " "

    message = "MSG " + user + " " + message_top + foodchain

    # print "new message: %s len of message: %d" % (message2, len(message2))

    return message

def server_port_is_in_use():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.bind((RPI_SERVER_IP, PORT))
    except socket.error as e:
        if e.errno == 98:
            s.close()
            return True
        else:
            # something else raised the socket.error exception
            sys.stdout.write("SOCKET ERROR: %s\n" % e)

    s.close()
    return False