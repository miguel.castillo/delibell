# consumers.py

from channels import Group
import sys

# Connected to websocket.receive
def ws_message(message):
    # ASGI WebSocket packet-received and send-packet message types
    # both have a "text" key for their textual data.
    # message.reply_channel.send({
    #     "text": message.content['text'],
    # })
    # Group("chat").send({
    #     "text": "[user] %s" % message.content['text'],
    #     })
    sys.stdout.write("[WEBSOCKET] received from websocket: %s\n" % message.content['text'])

# Connected to websocket.connect
def ws_add(message):
    # Accept the incoming connection
    message.reply_channel.send({"accept": True})
    # Add them to the chat group
    Group("delibellers").add(message.reply_channel)
    # print "Websocket Connected!"

# Connected to websocket.disconnect
def ws_disconnect(message):
    Group("delibellers").discard(message.reply_channel)
