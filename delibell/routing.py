# routing.py

from channels.routing import route
from place_order.consumers import ws_message, ws_add, ws_disconnect

channel_routing = [
    route('websocket.receive', ws_message),
    route('websocket.connect', ws_add),
    route('websocket.disconnect', ws_disconnect),
]
