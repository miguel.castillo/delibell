"""delibell URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from signup import views as signup_views
from place_order import views as order_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'', include('place_order.urls')),
    url(r'^$', order_views.place_order, name='place_order'),
    url(r'^confirm_order/', order_views.confirm_order, name='confirm_order'),
    url(r'^wait_order/', order_views.wait_order, name='wait_order'),
    url(r'^cancel_order/', order_views.cancel_order, name='cancel_order'),
    url(r'^order_arrived/', order_views.order_arrived, name='order_arrived'),
    url(r'^remove_order/', order_views.remove_order, name='remove_order'),
    url(r'^wrong_order/', order_views.wrong_order, name='wrong_order'),
    url(r'^about/', include('about.urls')),
    url(r'^myaccount/', include('myaccount.urls')),
    url(r'^login/', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'logout.html'}, name='logout'),
    url(r'^signup/$', signup_views.signup, name='signup'),
    url(r'^account_activation_sent/$', signup_views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', signup_views.activate, name='activate'),

]
