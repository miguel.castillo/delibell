# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect

from signup.models import Profile

@login_required
def myaccount(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return render(request, 'myaccount.html', {
                'form': form,
                'changed_password': True,
            })
    else:
        form = PasswordChangeForm(request.user)
    user_profile = Profile.objects.get(user=request.user)
    return render(request, 'myaccount.html', {
        'form': form,
        'changed_password': False,
        'has_order': user_profile.has_order,
    })
