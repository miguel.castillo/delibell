# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from signup.models import Profile

# Create your views here.
def about(request):
	if request.user.is_authenticated:
		user_profile = Profile.objects.get(user=request.user)
		return render(request, 'about.html', {
			'has_order': user_profile.has_order
			})
	else:
		return render(request, 'about.html', {
			'has_order': False,
			})
