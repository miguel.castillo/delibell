# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-13 03:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('signup', '0005_auto_20170511_0207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='order_message',
            field=models.CharField(default='', max_length=80),
        ),
    ]
