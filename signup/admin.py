# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Profile

# Register your models here.
class ProfileAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields': [
			'user',
			'email_confirmed',
			]}),
		('Order Information',{'fields': [
			'order_name',
			'order_room',
			'order_foodchain',
			'order_message'
			]}),
		('Flags',{'fields': [
			'has_order',
			'order_sent_to_rpi',
			'order_seen_by_rpi',
			'order_arrived'
			]}),
	]
	list_display = (
		'user',
		'email_confirmed',
		'order_name',
		'order_room',
		'order_foodchain',
		'order_message',
		'has_order',
		'order_sent_to_rpi',
		'order_seen_by_rpi',
		'order_arrived',
	)
	list_filter = [
		'has_order',
	]

	# fields = [
	# 	'user',
	# 	'email_confirmed',
	# 	'order_name',
	# 	'order_room',
	# 	'order_foodchain',
	# 	'order_message',
	# 	'has_order',
	# 	'order_sent_to_rpi',
	# 	'order_seen_by_rpi',
	# 	'order_arrived',
	# ]

admin.site.register(Profile, ProfileAdmin)