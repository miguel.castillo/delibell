# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    order_name = models.CharField(max_length=10,default='')
    order_room = models.CharField(max_length=6,default='')
    order_foodchain = models.CharField(max_length=16,default='')
    order_message = models.CharField(max_length=80,default='')
    # Flags
    has_order = models.BooleanField(default=False)
    order_sent_to_rpi = models.BooleanField(default=False)
    order_seen_by_rpi = models.BooleanField(default=False)
    #order_deleted_by_rpi = models.BooleanField(default=False)
    order_arrived  = models.BooleanField(default=False)
    def __str__(self):
    	return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
