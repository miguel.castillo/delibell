import logging
import time
import os

from delibell.settings import BASE_DIR

global ws_logger

def ws_log_init():
    global ws_logger
    ws_logger = logging.getLogger('ws_server')
    ws_logger.setLevel(logging.DEBUG)
    path = os.path.join(BASE_DIR, 'logs/ws_server.log')
    ws_log_file = logging.FileHandler(path)
    ws_log_file.setLevel(logging.DEBUG)
    ws_log_format = logging.Formatter('%(message)s')
    ws_log_file.setFormatter(ws_log_format)
    ws_logger.addHandler(ws_log_file)

def ws_server_log(message):
    global ws_logger
    ws_logger.debug('[ %s ] %s' % (getCurrTime(),message))

def getCurrTime():
    currTime = time.strftime("%a %d %b %Y %H:%M:%S", time.localtime())
    return currTime
