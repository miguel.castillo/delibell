import logging
import time
import os

from delibell.settings import BASE_DIR

global rpi_logger

def rpi_log_init():
    global rpi_logger
    rpi_logger = logging.getLogger('rpi_server')
    rpi_logger.setLevel(logging.DEBUG)
    path = os.path.join(BASE_DIR, 'logs/rpi_server.log')
    rpi_log_file = logging.FileHandler(path)
    rpi_log_file.setLevel(logging.DEBUG)
    rpi_log_format = logging.Formatter('%(message)s')
    rpi_log_file.setFormatter(rpi_log_format)
    rpi_logger.addHandler(rpi_log_file)

def rpi_server_log(message):
    global rpi_logger
    rpi_logger.debug('[ %s ] %s' % (getCurrTime(),message))

def getCurrTime():
    currTime = time.strftime("%a %d %b %Y %H:%M:%S", time.localtime())
    return currTime
